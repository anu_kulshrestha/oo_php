<?php

require __DIR__ . '/../../config.php';

//what is a class
//
//A class is a blueprint for making someing
//The things we make from classes are called objects
//Properties define class 
//Accessibility defines how the objects can use these properties - public, private and protected
//Public - 
//Private - 
//Protected - 
//To make an object from a class, we must INSTANTIATE the class - making it real
//Methods are the functions in the class
//Inside the class any private public or protected methods and properties are accessible 
//from anywhere in the class.
class Car
{
    public $make = 'Honda';
    public $model = 'Civic';
    public $price = 25000;
    public $color = 'blue';
    public $year = 2020;

    /**
     * [blowHorn description]
     * @return string [description]
     */
    public function blowHorn()
    {
        return 'Honkkkkkkk!';
    }

    /**
     * [startEngine description]
     * @return string [description]
     */
    private function startEngine()
    {
        return 'Starting Engine';
    }

    public function backOutOfGarage()
    {
        $horn = $this->blowHorn();
        $engine = $this->startEngine();
        return $horn . '...' . $engine;
    }

}
//INSTANTIATE The class
$car1 = new Car();
$car2 = new Car();

dump_continue($car1);

$car2->make = 'Chevrolete';
$car2->model = 'Malibu';
$car2->price = 27500;
$car2->color = 'Chevrolete';
$car2->year = 2020;

dump_continue($car2);

dump_continue($car1->blowHorn());
dump_continue($car2->blowHorn());

dump_continue($car1->backOutOfGarage());
dump_continue($car2->backOutOfGarage());
