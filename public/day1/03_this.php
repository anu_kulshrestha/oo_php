<?php

require __DIR__ . '/../../config.php';

// What is a $this?
// 
class Book
{
    private $cost;
    public $publication = 'Unknown';
    //class properties can be declared without initialization or assigning a value
    //but they need to be given accessibility type
    public function about()
    {
        return $this->title . ', by ' . $this->author;
    }
    public function me()
    {
        return $this;
    }
}

$dune = new Book();
$harry_potter = new Book();
$the_shining = new Book();


dump_continue($dune);
dump_continue($harry_potter);
dump_continue($the_shining);

// the properties that are declared outside the class are public
$dune->title = 'Dune';
$dune->author = 'Frank Herbert';

$harry_potter->title = 'Harry Potter and The Goblet of Fire';
$harry_potter->author = 'J.K. Rowling';

$the_shining->title = 'The Shining';
$the_shining->author = 'Stephen King';


dump_continue($dune);
dump_continue($harry_potter);
dump_continue($the_shining);

dump_continue($dune->about());
dump_continue($harry_potter->about());
dump_continue($the_shining->about());

// $dune->sayHello = function() {
//     return 'Hi!';
// }; //anonymous function needs to be ended with a semicolon

//dump_continue($dune->sayHello());


//To access property outside class $dune->title. Inside class $this->title.

//The me() method returns a reference to the object
dump_continue($dune->me());
dump_continue($harry_potter->me());
dump_continue($the_shining->me());

//When we have a reference to the object, we can chain additional methods to it.
//this is called method chaining
dump_continue($dune->me()->about());
dump_continue($harry_potter->me()->about());
dump_continue($the_shining->me()->about());

//dump_continue($dune->me()->cost); // you CANNOT access INSIDE OUT private property
dump_continue($dune->me()->publication); //BUT you CAN access INSIDE OUT public property


