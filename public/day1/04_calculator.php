<?php

require __DIR__ . '/../../config.php';

class Calculator
{
    private $total =0;

    //add
    public function add($num)
    {
        $this->total += $num;
    }
        
    //substract
    public function minus($num)
    {
        $this->total -= $num;
    }

    //multiply
    public function mult($num)
    {
        $this->total *= $num;
    }

    //divide
    public function div($num)
    {
        if($num!==0) {
            $this->total /= $num;
        }
    }

    public function total()
    {
        return $this->total;
    }
}


$c = new Calculator(); // if no __construct then you may avoide parenthesis.

$c->add(25);
$c->minus(5);
$c->mult(5);
$c->div(5);

$c1= new Calculator;

$c1->add(45);
$c1->minus(5);
$c1->mult(5);
$c1->div(5);
dump_continue($c->total());
dump_continue($c1->total());