<?php

require __DIR__ . '/../../config.php';

class Calculator
{
    private $total =0;
    private $tape;
    //add
    public function add($num)
    {
        $this->total += $num;
        $this->tape .= "<br/> Add $num ";
        return $this;
    }
        
    //substract
    public function minus($num)
    {
        $this->total -= $num;
        $this->tape .= "<br/> Substract $num ";
        return $this;
    }

    //multiply
    public function mult($num)
    {
        $this->total *= $num;
        $this->tape .= "<br/> Multiply by $num ";
        return $this;
    }

    //divide
    public function div($num)
    {
        if($num!==0) {
            $this->total /= $num;
            $this->tape .= "<br/> Divide by $num ";
            return $this;
        }
    }

    public function clear()
    {
        $this->total=0;
    }

    public function total()
    {

        return $this->total;
    }

    public function tape()
    {
        $tape = $this->tape . "<br />---------------------------<br />";
        $tape .= "Total: " . $this->total;
        return $tape;
    }
}


$c = new Calculator(); // if no __construct then you may avoide parenthesis.

//dump_continue($c->add(25)->minus(5)->mult(5)->div(5)->total());

$c->clear();

$c->add(6)
    ->mult(11);

//dump_continue($c->total());


$c->clear();
$c->add(6)
    ->mult(14)
    ->minus(22)
    ->add(4)
    ->div(2)
    ->total();

dump_continue($c->tape());




