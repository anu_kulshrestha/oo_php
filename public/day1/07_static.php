<?php
require __DIR__ . '/../../config.php';

class Vehicle
{
    static protected $count = 0; //the static variable belongs to class and not to object
    protected $make;
    protected $model;

    public function __construct($make,$model)
    {
        $this->make = $make;
        $this->model = $model;
        self::$count += 1;
    }

    public function count()
    {
        return self::$count;
    }
}

$car1 = new Vehicle('Honda', 'Civic');
$car2 = new Vehicle('Chevy', 'Malibu');
$car3 = new Vehicle('AMC', 'Jeep');
$motorcycle1 = new Vehicle('Harley Davidson', 'Electra Glide');


dump_continue($car1);
dump_continue($car2);
dump_continue($car3);
dump_continue($motorcycle1);

dump_continue($car1->count());
dump_continue($car2->count());
dump_continue($car3->count());
dump_continue($motorcycle1->count());

//dump_continue(Vehicle::$count);

