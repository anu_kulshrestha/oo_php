<?php
require __DIR__ . '/../../config.php';
class Person
{
    public $name;
    private $age;
    protected $gender;

    public function __construct($name,$age,$gender)
    {
        $this->name = $name;
        $this->age = $age;
        $this->gender = $gender;
    }
}

class User extends Person
{
    public function getName()
    {
        return $this->name;
    }

    public function getAge()
    {
        return $this->age;
    }

    public function getGender()
    {
        return $this->gender;
    }
}

$x = new Person('Sally',22,'Female');

dump_continue($x->name);
//dump_continue($x->gender);
//dump_continue($x->age);

$y = new User('Banu',26,'Male');
dump_continue($y->name);
dump_continue($y);
//dump_continue($y->getAge());