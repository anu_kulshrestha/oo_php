<?php
use AbstractModel;
use BookModel;
class MyFoo
{
    public function __construct(AbstractModel $model)
    {
        $this->model=$model;
    }
    
    public function fruits(array $fruits)
    {
        $this->fruits = $fruits;
    }

    public function one(int $id)
    {
        $this->id = $id;
    }

    public function test(object $x)
    {
        $this->x=$x;

    }
}