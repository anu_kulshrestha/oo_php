<?php

// S - Single Responsibility Principle
// O - Open for Extension, Closed for Modification
// L - Liskov Substitution Principle
// I - Interface Segregation Principle
// D

class PageController
{
    function save(IInput $input)
    {

    }

}


interface IInput
{
   // public function get($field = null);
   //  public function post($field = null);
   //   public function cookie($field = null);
   //    public function request($field = null);
} 

interface IInputGet
{
     public function get($field = null);
}

interface IInputPost
{
     public function post($field = null);
}

interface IInputCookie
{
     public function Cookie($field = null);
}

interface IInputRequest
{
     public function request($field = null);
}

class Post implements IInput, IInputPost
{
    public function post($field = null)
    {
        return "I AM Processing POST";
    }
}

$input = new Post();
$pages = new PageController($input);


