<?php
class Test
{
    public function __construct(IModel $model)
    {
        $this->model=$model;
    }
}

/**
 * default all functions are public. will work if you do not write. But not a good practice to avoid it.
 * Defines the public API of a class
 * Application public Interface
 * can only contain public methods
 * all methods must be abstract
 * abstract keyword need not to be mentioned.
 * Interfaces are contracts. If implementing the interface, follow the rules.
 */
interface IModel
{
    public function all() : array;
    public function one(int $id) : array;
    public function save(array $array) : int;
    public function update(array $array) : int;

}

class MyModel implements IModel
{
    public function all() : array
    {

    }

    public function one($id) : array
    {

    }

    public function save($array) : int
    {

    }

    public function delete($id) 
    {

    }
    
    public function update($array) : int
    {

    }

}

$model = new MyModel();

$test = new Test($model); // should work because we are implementing IModel

var_dump($test);