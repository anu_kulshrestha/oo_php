<?php
ob_start(); //enable output buffering to avoid innocent or unknowing spaces
session_start(); //start session
//use App\Models\Model;
//extract errors and post values from session
$errors = $_SESSION['errors'] ?? [];
$post = $_SESSION['post'] ?? [];
//unset the session superglobal variable
unset($_SESSION['errors']);
unset($_SESSION['post']);

//DB credentials
define('DB_DSN', 'mysql:hostname=localhost;dbname=booksite');
define('DB_USER','book_user');
define('DB_PASS', 'mypass');

//$conn
$dbh = new PDO(DB_DSN,DB_USER,DB_PASS);
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


//Model::init($dbh);





//calling function file
require __DIR__ . '/functions.php';


//require __DIR__ . '/validators.php'; //commenting this for Validator.php in Lib is now used

//dump_die($dbh); //dump to check if there is any error

//In JS same code might look like this
// var dbh = new PDO(DB_DSN,DB_USER,DB_PASS);
// dbh.setAttribute();