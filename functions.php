<?php
//Utility functions

/**
 * @param Mixed $var variable to dump [type] $[name] [<description>]
 * @return void [<description>]
 */

function dump_continue($var)
{
	echo '<pre>';
	var_dump($var);
	echo '</pre>';
}


/**
 * @param Mixed $var variable to dump [type] $[name] [<description>]
 * @return void [<description>]
 */

function dump_die($var)
{
	echo '<pre>';
	var_dump($var);
	echo '</pre>';
	die; //kills the script right there
}


/**
 * @param  string $str
 * @return string
 */
function e($str)
{
	//ENT_QUOTES - if there is any quotes in it convert it to entities.
	//html entities will convert any executable code into entities
	return htmlentities($str, ENT_QUOTES,"UTF-8");
}
?>