<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;
use App\Models\Model;
use App\Models\BookModel;

final class BookModelTest extends TestCase
{
    protected $bookModel;
    public function setup()
    {
        $dbh = new PDO('mysql:hostname=localhost;dbname=booksite','book_user','mypass');

        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        Model::init($dbh);

        $this->bookModel = new BookModel();
    }

    public function testGetAllBooksReturnsArray()
    {
        $model = $this->bookModel;
        $books = $model->all();
        $this->assertIsArray($books);
    }

    public function testGetAllBooksContainsArrayOfBooks()
    {
        $model = $this->bookModel;
        $books = $model->all();
        $this->assertArrayHasKey('title',$books[0]);
    }

    public function testGetOneBooksReturnsArrayOfOneBooks()
    {
        $model = $this->bookModel;
        $book = $model->one(1);
        $this->assertIsArray($book);
    }
}
