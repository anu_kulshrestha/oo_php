<?php
use PHPUnit\Framework\TestCase;
use App\Models\Model;
use App\Models\PublisherModel;

final class PublisherModelTest extends TestCase
{
    protected $publisherModel;

    public function setup()
    {
        $dbh = new PDO('mysql:hostname=localhost;dbname=booksite','book_user','mypass');

        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        Model::init($dbh);

        $this->publisherModel = new PublisherModel();
    }

    public function testGetAllPublishersReturnsArray()
    {
        $model = $this->publisherModel;
        $publishers = $model->all();
        $this->assertIsArray($publishers);
    }

    public function testGetAllPublishersContainsArrayOfPublishers()
    {
        $model = $this->publisherModel;
        $publishers = $model->all();
        $this->assertArrayHasKey('name',$publishers[0]);
    }

    public function testGetOnePublisherReturnsArrayOfOnePublisher()
    {
        $model = $this->publisherModel;
        $publisher = $model->one(1);
        $this->assertIsArray($publisher);
    }
}