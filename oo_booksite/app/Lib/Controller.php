<?php

namespace App\Lib;

abstract class Controller
{
    protected static $view_path = '';

    public static function init($path)
    {
        self::$view_path = $path;
    }
    protected function view($view, $data=[])
    {
        //View::load($view,$data);
        //
        //extract key/value pairs to variables in current scope
        extract($data);
        require self::$view_path . "/{$view}.view.php";
    }
}