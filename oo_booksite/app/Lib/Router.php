<?php
namespace App\Lib;

class Router
{
    private $request;
    private $method;
    private $get = [];
    private $post = [];
    private $put_array = [];
    private $delete_array=[];

    public function __construct(string $request,string $method) 
    {
        $this->request=trim($request,'/');
        $this->method=$method;
    }

    public function get(string $route,callable $callable)
    {
        //callable means executable
        $clean_route = trim($route,'/');
        $this->get[$clean_route] = $callable;
    }

    public function dispatch()
    {
        foreach ($this->get as $route => $callable) {
            # code...
            if($route === $this->request) {
                call_user_func($callable);
                die;
            }
        }

        http_response_code(404);
        die('404 - NOT FOUND');
    }

}