<?php

namespace App\Lib;

class Validator
{
    private $post = [];
    private $errors = [];

    public function __construct($array)
    {
        $this->post=$array;
    }


    private function label($field_name)
    {
        return ucwords(str_replace('_', ' ', $field_name));
    }

    /**
     * validate required fields
     * @param  array $required array of the required field 
     */

    public function required($required) //called signature of the function
    {
        foreach($required as $key)
        if(strlen($this->post[$key])==0) {
            $this->errors[$key][] = $this->label($key) . ' is required';
        }   
    }

    public function email_check($field)
    {
        $email=$this->post[$field];
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->errors[$field][] = $this->label($field) . " is not a valid email address";
        }
    }

    public function match_val($key1,$key2)
    {
        $pwd = $this->post[$key1];
        $pwd_confirm = $this->post[$key2];
        if($pwd!=$pwd_confirm) {
            $this->errors[$key1][] = $this->label($key1) . " does not match";
        }
    }

    public function length_check($field,$min,$max)
    {
        $var_length=strlen($this->post[$field]);
        if($var_length<$min || $var_length>$max) {
            $this->errors[$field][] = $this->label($field) . " should be minimum {$min} or maximum {$max} character long";
        }
    }

    public function validate_number($field,$min,$max)
    {
        $var=$this->post[$field];
        if(!is_numeric($var)) {
            $this->errors[$field][] = $this->label($field) . " should be a number";
        } elseif($var<$min || $var>$max) {
            $this->errors[$field][] = $this->label($field) . " should be minimum {$min} or maximum {$max} age";
        }
    }

    public function check_post($field)
    {
        $pattern = '/([A-z]{1}[\d]{1}[A-z]{1})[- ]?([\d]{1}[A-z]{1}[\d]{1})/';
        if(preg_match($pattern, $this->post[$field], $matches)===0) {
            $this->errors[$field][] = $this->label($field) . " is not a valid postal code";
        }
        if(preg_match($pattern, $this->post[$field], $matches)===false) {
            $this->errors[$field][] = $this->label($field) . " got some error";
        }
    }

    public function errors()
    {
        return $this->errors;
    }
}