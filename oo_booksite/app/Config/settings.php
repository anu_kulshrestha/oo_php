<?php
//no trailing slash on paths
return array(
    'DB_DSN' => realpath(__DIR__ . '/../Storage/database.sqlite'),
    'SITE_NAME' => 'Book Site',
    'REQUEST_URI' => $_SERVER['REQUEST_URI'],
    'REQUEST_METHOD' => $_SERVER['REQUEST_METHOD'],
    'VIEW_PATH' => realpath(__DIR__ . '/../Views'),
    'BASE_PATH' => realpath(__DIR__ . '/../..'),
    'ROUTES_FILE' => realpath(__DIR__ . '/route.php')
);