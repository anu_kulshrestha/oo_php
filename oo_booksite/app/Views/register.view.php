<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>About us</title>
	<link rel="stylesheet" type="text/css" href="css/style.css?v=2865" />
</head>
<body>

	<div id="header">

	<nav>
		<img id="logo" src="images/logo.jpg" alt="logo" />
		<?php require __DIR__ . '/includes/topnav.inc.php';?>

	</nav>

	</div><!-- /#header -->

<div class="container">

	
	<div class="header_img">
		<img src="images/header.jpg" alt="header" />
	</div>

	<div class="content">
        <h1>Register</h1>

        <!-- <?=dump_continue($_SERVER['REQUEST_METHOD']);?>
        <?=dump_continue($errors); dump_continue($post);?> -->

        <p>Fields with a red <span class="required"></span> are required.</p>
        <form action="/register" method="post" novalidate>
            <fieldset>
                <legend>Register</legend>
            <p> <label class="required" for="first">First name</label>
                <input type="text" name="first" value="<?=$post['first'] ?? '' ?>" />
                <?=$errors['first'][0] ?? ''?>
            </p>
            <p> <label class="required" for="last">Last Name</label>
                <input type="text" name="last" value="<?= $post['last'] ?? '' ?>" /></p>
                <?=$errors['last'][0] ?? ''?>
            <p> <label class="required" for="age">Your Age</label>
                <input type="text" name="age" value="<?= $post['age'] ?? '' ?>" /></p>
                <?=$errors['age'][0] ?? ''?>
            <p> <label class="required" for="email">Email Address</label>
                <input type="email" name="email" value="<?= $post['email'] ?? '' ?>" />
                <?=$errors['email'][0] ?? ''?>
            </p>
            <p> <label class="required" for="postal_code">Postal Code</label>
                <input type="text" name="postal_code" value="<?=$post['postal_code'] ?? '' ?>" />
                <?=$errors['postal_code'][0] ?? ''?>
            </p>
            <p> <label class="required" for="password">Password</label>
                <input type="password" name="password">
                <?=$errors['password'][0] ?? ''?>
            </p>
            <p> <label class="required" for="pwd_confirm">Confirm Password</label>
                <input type="password" name="pwd_confirm">
                <?=$errors['pwd_confirm'][0] ?? ''?>
            </p>
            <p> <button type="submit">Submit</button>
            </p>
            </fieldset>
        </form>

</div><!-- /content -->


</div><!-- /.container -->

<div id="footer">

	<?php require __DIR__ . '/includes/footernav.inc.php';?>

	<p>Content copyright by Big Book Site - all rights reserved.</p>

</div><!-- /footer -->
	
</body>
</html>


