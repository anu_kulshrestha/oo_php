<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>About us</title>
    <link rel="stylesheet" type="text/css" href="css/style.css?v=2865" />
</head>
<body>

    <div id="header">

    <nav>
        <img id="logo" src="images/logo.jpg" alt="logo" />
       <?php require __DIR__ . '/includes/topnav.inc.php';?>

    </nav>

    </div><!-- /#header -->

<div class="container">

    
    <div class="header_img">
        <img src="images/header.jpg" alt="header" />
    </div>

    <div class="content">

      <h1>404 - Not Found</h1>
      <h2>Oops! We could not find what you are looking for</h2>
      <p>Please browse our site, or use the search function, to find what you arae looking for.</p>
    </div><!-- /content -->


</div><!-- /.container -->

<div id="footer">

    <?php require __DIR__ . '/includes/footernav.inc.php';?>

    <p>Content copyright by Big Book Site - all rights reserved.</p>

</div><!-- /footer -->
    
</body>
</html>


