<?php

namespace App;

use App\Lib\Router;
use App\Lib\Controller;

class App
{
    private $c;
    private $route;

    public function __construct(\Pimple\Container $c)
    {
        $this->c=$c;

        Controller::init($c['VIEW_PATH']);

        //var_dump('APP CONSTRUCTED');        

        $this->route = new Router($c['REQUEST_URI'],$c['REQUEST_METHOD']);
        //Router::init($_SERVER['REQUEST_METHOD'],$_SERVER['REQUEST_URI']);
        //Router::init($this->c['REQUEST_METHOD'],$this->c['REQUEST_URI']);
        //
        $this->loadRoutes($this->route);
    }

    public function loadRoutes($route)
    {
        require $this->c['ROUTES_FILE'];
    }

    public function run()
    {
        $this->route->dispatch();
    }
}