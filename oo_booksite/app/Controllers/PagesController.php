<?php

namespace App\Controllers;

class PagesController extends \App\Lib\Controller
{
    public function show(string $page, string $title, string $slug)
    {
        $title=ucfirst($page);
        $slug=strtolower($page);
        $this->view($page,compact('title','slug'));
    }


}