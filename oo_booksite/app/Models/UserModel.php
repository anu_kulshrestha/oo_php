<?php

namespace App\Models;

class UserModel extends Model
{
    protected $table='user_registration';
    protected $key='id';


    /**
     * [saveUser - save details from user_registration page into database]
     * @param  string $user [$_POST values]
     * @return integer lastInsertId  [primary key of the record]
     */
    public function saveUser($user) 
    {
        $query = "Insert Into user_registration
                    (
                      first_name,
                      last_name,
                      street_details,
                      city_name,
                      province_name,
                      country_name,
                      postal_code, 
                      email,
                      phone,
                      user_type,
                      user_password,
                      created_at,
                      updated_at
                    )
                  Values
                    (
                      :first_name, 
                      :last_name, 
                      :street_details, 
                      :city_name, 
                      :province_name, 
                      :country_name, 
                      :postal_code, 
                      :email, 
                      :phone, 
                      :user_type, 
                      :user_password, 
                      NOW(), 
                      NOW()
                    )"; 
        //place holders beginning with :
         
        $stmt = self::$dbh->prepare($query);
        
        $params = array(
            ':first_name'=> $user['first_name'],
            ':last_name'=> $user['last_name'],
            ':street_details'=> $user['street_details'],
            ':city_name'=> $user['city_name'],
            ':province_name'=> $user['province_name'],
            ':country_name'=> $user['country_name'],
            ':postal_code' => $user['postal_code'],
            ':email' => $user['email'],
            ':phone' => $user['phone'],       
            ':user_type' => 'customer',
            ':user_password' => password_hash($user['user_password'], PASSWORD_DEFAULT)
        );
        
        $stmt->execute($params);

        return self::$dbh->lastInsertID();       
    }


    /**
     * [getUser - get user details for profile page]
     * @param  int $userid [user id]
     * @param  string $dbh [database connection variable]
     * @return array [details of user from 4 tables user_registration, 
     *                country_names, province names, city_names]
     */
    public function one($userid) 
    {        
        $query = 'SELECT 
                    u.first_name as "First Name",
                    u.last_name as "Last Name",
                    u.email as "Email",
                    u.phone as "Mobile",
                    u.street_details as "Address",
                    u.postal_code as "Postal Code",
                    u.city_name as City,
                    u.province_name as Province,
                    u.country_name as Country,
                    u.created_at as "Registered On",
                    u.updated_at as "Updated On",
                    u.user_password as "User Password"           
                    FROM 
                    user_registration u
                    WHERE 
                    u.id = :userid';

        $stmt = self::$dbh->prepare($query);

        $params = array(
            ':userid' => $userid
        );
        
        $stmt->execute($params);
        
        //fetch single result as an associative array
        return $stmt->fetch(\PDO::FETCH_ASSOC);        
    }


    /**
     * [checkUser - get details from user_registration page for email id entered]
     * @param  string $email [email id entered on login page]
     * @return array [details of user]
     */
    public function checkUser($email)
    {
        $query = 'SELECT * FROM user_registration WHERE email=:email';
        $stmt = self::$dbh->prepare($query);
        $params = array(
            ':email'=> $email
        );
        $stmt->execute($params);
        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    // public function delete($id)
    // {

    // }

    // public function create($array)
    // {

    // }

    // public function save($array)
    // {
        
    // }
}