<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use App\Models\Model;
use App\Models\UserModel;

/**
 * @file - UserModelTest.php
 * @desc - Child class extended from class TestCase
 * @updated on - 2020-09-09
 */
final class UserModelTest extends TestCase
{
    // a protected property to instantiate the UserModel class
    protected $userModel;

    /**
     * [setup - a function for this test file to create or establish
     * connection with the database and initialize the static proctected
     * property of Model class]
     */
    public function setup()
    {
        $dbh = new PDO('mysql:hostname=localhost;dbname=php_capstone','capstone_user','mycapstone');

        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        Model::init($dbh);

        $this->userModel = new UserModel();
    }

    /**
     * [testGetAllUsersReturnsArray a test function to check if there is
     * a function that gets all the record.]
     */
    public function testGetAllUsersReturnsArray()
    {
        $model = $this->userModel;
        $users = $model->all();
        $this->assertIsArray($users);
    }

    /**
     * [testGetAllUserssContainsArrayOfUsers -a test function to
     * check if the all function for all records of the table 
     * has a column name existing]
     */
    public function testGetAllUsersContainsArrayOfUsers()
    {
        $model = $this->userModel;
        $users = $model->all();
        $this->assertArrayHasKey('first_name',$users[0]);
    }

    /**
     * [testGetOneUserReturnsArrayOfOneUser -a test function to 
     * check if a particular record search function exists]
     */
    public function testGetOneUserReturnsArrayOfOneUser()
    {
        $model = $this->userModel;
        $user = $model->one(1);
        $this->assertIsArray($user);
    }

    /**
     * [testNonExistingEmailReturnsFalse - a test function to 
     * check if a particular email address does not exist.]
     */
    public function testNonExistingEmailReturnsFalse()
    {
        $model = $this->userModel;
        $user = $model->checkUser('mary@gmail.com');
        $this->assertFalse($user);
    }

    /**
     * [testExistingEmailReturnsUser - a test function to check
     * if a particular email address exists.]
     */
    public function testExistingEmailReturnsUser()
    {
        $model = $this->userModel;
        $user = $model->checkUser('anu@gmail.com');
        $this->assertIsArray($user);
    }
}
