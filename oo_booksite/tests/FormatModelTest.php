<?php
use PHPUnit\Framework\TestCase;
use App\Models\Model;
use App\Models\FormatModel;

final class FormatModelTest extends TestCase
{
    protected $formatModel;

    public function setup()
    {
        $dbh = new PDO('sqlite:' . __DIR__ . '/../app/Storage/database.sqlite');

        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        Model::init($dbh);

        $this->formatModel = new FormatModel();
    }

    public function testGetAllFormatsReturnsArray()
    {
        $model = $this->formatModel;
        $formats = $model->all();
        $this->assertIsArray($formats);
    }

    public function testGetAllFormatsContainsArrayOfFormats()
    {
        $model = $this->formatModel;
        $formats = $model->all();
        $this->assertArrayHasKey('name',$formats[0]);
    }

    public function testGetOneFormatReturnsArrayOfOneFormat()
    {
        $model = $this->formatModel;
        $format = $model->one(1);
        $this->assertIsArray($format);
    }
}