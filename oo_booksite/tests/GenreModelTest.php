<?php
use PHPUnit\Framework\TestCase;
use App\Models\Model;
use App\Models\GenreModel;

final class GenreModelTest extends TestCase
{
    protected $genreModel;

    public function setup()
    {
        $dbh = new PDO('sqlite:' . __DIR__ . '/../app/Storage/database.sqlite');

        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        Model::init($dbh);

        $this->genreModel = new GenreModel();
    }

    public function testGetAllGenresReturnsArray()
    {
        $model = $this->genreModel;
        $genres = $model->all();
        $this->assertIsArray($genres);
    }

    public function testGetAllGenresContainsArrayOfGenres()
    {
        $model = $this->genreModel;
        $genres = $model->all();
        $this->assertArrayHasKey('name',$genres[0]);
    }

    public function testGetOneGenreReturnsArrayOfOneGenre()
    {
        $model = $this->genreModel;
        $genre = $model->one(1);
        $this->assertIsArray($genre);
    }
}