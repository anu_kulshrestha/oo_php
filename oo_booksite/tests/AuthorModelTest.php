<?php
use PHPUnit\Framework\TestCase;
use App\Models\Model;
use App\Models\AuthorModel;

final class AuthorModelTest extends TestCase
{
    protected $authorModel;

    public function setup()
    {
        $dbh = new PDO('sqlite:' . __DIR__ . '/../app/Storage/database.sqlite');

        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        Model::init($dbh);

        $this->authorModel = new AuthorModel();
    }

    public function testGetAllAuthorsReturnsArray()
    {
        $model = $this->authorModel;
        $authors = $model->all();
        $this->assertIsArray($authors);
    }

    public function testGetAllAuthorsContainsArrayOfAuthors()
    {
        $model = $this->authorModel;
        $authors = $model->all();
        $this->assertArrayHasKey('country',$authors[0]);
    }

    public function testGetOneAuthorReturnsArrayOfOneAuthor()
    {
        $model = $this->authorModel;
        $author = $model->one(1);
        $this->assertIsArray($author);
    }
}