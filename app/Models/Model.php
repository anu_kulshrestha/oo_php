<?php
namespace App\Models;
abstract class Model
{
    static protected $dbh; // one and only one and shared to child class
    protected $table;
    protected $key;
    static public function init($dbh)
    {
        self::$dbh=$dbh;
    }

    public function all()
    {
        $query = "SELECT * FROM {$this->table}";
        $stmt = self::$dbh->prepare($query);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function one($id)
    {
        $query = "SELECT * FROM {$this->table} WHERE {$this->key} = :id";
        $stmt = self::$dbh->prepare($query);
        $params = array(':id' => $id);
        $stmt->execute($params);
        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }


    // abstract public function delete($id);
    // abstract public function create($array);
    // abstract public function save($array);
}