<?php
namespace App\Models;

class BookModel extends Model
{
    public function all()
    {
        $query = 'SELECT book.*, 
                author.name as author, 
                publisher.name as publisher,
                format.name as format, 
                genre.name as genre
                from book
                join author on author.author_id=book.author_id
                join publisher on publisher.publisher_id = book.publisher_id
                join format on format.format_id=book.format_id
                join genre on genre.genre_id=book.genre_id
                order by book.title
                asc';
        $stmt = self::$dbh->prepare($query); //coming from its parent class where it is a static protected variable.
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function one($id)
    {
        $query = 'SELECT book.*, 
                author.name as author, 
                publisher.name as publisher,
                format.name as format, 
                genre.name as genre
                FROM book
                join author on author.author_id=book.author_id
                join publisher on publisher.publisher_id = book.publisher_id
                join format on format.format_id=book.format_id
                join genre on genre.genre_id=book.genre_id
                WHERE book_id = :book_id';
        $stmt = self::$dbh->prepare($query);
        $params = array(':book_id' => $id);
        $stmt->execute($params);
        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    // public function delete($id)
    // {

    // }

    // public function create($array)
    // {

    // }

    // public function save($array)
    // {
        
    // }
}